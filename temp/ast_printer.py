from token_type import TokenType
from tokens import Tokens
from expr import Visitor, Binary, Unary, Literal, \
    Grouping


class AstPrinter(Visitor):
    def print(self, expr):
        return expr.accept(self)


    def parenthesize(self, name, *exprs):
        string_builder = f'({name}'
        for expr in exprs:
            string_builder += f' {expr.accept(self)}'
        string_builder += ')'
        return string_builder


    def visit_binary_expr(self, expr):
        return self.parenthesize(expr.operator.lexeme, expr.left, expr.right)


    def visit_grouping_expr(self, expr):
        return self.parenthesize('group', expr.expression)


    def visit_literal_expr(self, expr):
        if expr.value is None:
            return 'nil'
        return str(expr.value)


    def visit_unary_expr(self, expr):
        return self.parenthesize(expr.operator.lexeme, expr.right)


if __name__ == '__main__':
    expression = Binary(
        Unary(
            Tokens(TokenType.MINUS, '-', None, 1),
            Literal(123)
        ),
        Tokens(TokenType.STAR, '*', None, 1),
        Grouping(
            Literal(45.67)
        )
    )
    print(AstPrinter().print(expression))

