import sys

import error_reporter
from scanner import Scanner


def run(source):
    scanner = Scanner(source)
    tokens = scanner.scan_tokens()
    for token in tokens:
        print(token)


def run_file(path):
    with open(path, 'r', encoding='utf-8') as f:
        source = f.read()
    run(source)
    if error_reporter.had_error:
        sys.exit(65)


def run_prompt():
    while True:
        try:
            print('> ', end='')
            line = input()
            if not line:
                break
            run(line)
            error_reporter.had_error = False
        except EOFError:
            print('')
            break


if __name__ == '__main__':
    if len(sys.argv) > 2:
        print('Usage: python3 lox.py [script]')
        sys.exit(64)
    elif len(sys.argv) == 2:
        run_file(sys.argv[1])
    else:
        run_prompt()

