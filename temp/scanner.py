from token_type import TokenType
from tokens import Tokens
from error_reporter import error


class Scanner():
    tokens = []
    start = 0
    current = 0
    line = 1
    keywords = {
        'and': TokenType.AND,
        'class': TokenType.CLASS,
        'else': TokenType.ELSE,
        'false': TokenType.FALSE,
        'for': TokenType.FOR,
        'fun': TokenType.FUN,
        'if': TokenType.IF,
        'or': TokenType.NIL,
        'print': TokenType.PRINT,
        'return': TokenType.RETURN,
        'super': TokenType.SUPER,
        'this': TokenType.THIS,
        'true': TokenType.TRUE,
        'var': TokenType.VAR,
        'while': TokenType.WHILE
    }


    def __init__(self, source):
        self.source = source


    def is_at_end(self):
        return type(self).current >= len(self.source)


    def advance(self):
        char = self.source[type(self).current]
        type(self).current += 1
        return char


    def add_token(self, token_type, literal):
        text = self.source[type(self).start: type(self).current]
        type(self).tokens.append(Tokens(token_type, text, literal, type(self).line))


    def match(self, expected):
        if self.is_at_end():
            return False
        if self.source[type(self).current] != expected:
            return False
        type(self).current += 1
        return True


    def peek(self):
        if self.is_at_end():
            return '\0'
        return self.source[type(self).current]


    def string(self):
        while self.peek() != '"' and not self.is_at_end():
            if self.peek() == '\n':
                type(self).line += 1
            self.advance()
        if self.is_at_end():
            error(type(self).line, 'Unterminated string.')
            return
        self.advance()
        value = self.source[type(self).start+1: type(self).current-1]
        self.add_token(TokenType.STRING, value)


    def is_digit(self, c):
        return '0' <= c <= '9'


    def peek_next(self):
        if type(self).current + 1 >= len(self.source):
            return '\0'
        return self.source[type(self).current+1]


    def number(self):
        while self.is_digit(self.peek()):
            self.advance()
        if self.peek() == '.' and self.is_digit(self.peek_next()):
            self.advance()
            while self.is_digit(self.peek()):
                self.advance()
        self.add_token(TokenType.NUMBER, float(self.source[type(self).start: type(self).current]))


    def is_alpha(self, c):
        return 'a' <= c <= 'z' or 'A' <= c <= 'Z' or c == '_'


    def is_alphanumeric(self, c):
        return self.is_alpha(c) or self.is_digit(c)


    def identifier(self):
        while self.is_alphanumeric(self.peek()):
            self.advance()
        text = self.source[type(self).start: type(self).current]
        try:
            token_type = type(self).keywords[text]
        except KeyError:
            token_type = TokenType.IDENTIFIER
        self.add_token(token_type, None)


    def scan_token(self):
        c = self.advance()
        match c:
            case '(':
                self.add_token(TokenType.LEFT_PAREN, None)
            case ')':
                self.add_token(TokenType.RIGHT_PAREN, None)
            case '{':
                self.add_token(TokenType.LEFT_BRACE, None)
            case '}':
                self.add_token(TokenType.RIGHT_BRACE, None)
            case ',':
                self.add_token(TokenType.COMMA, None)
            case '.':
                self.add_token(TokenType.DOT, None)
            case '-':
                self.add_token(TokenType.MINUS, None)
            case '+':
                self.add_token(TokenType.PLUS, None)
            case ';':
                self.add_token(TokenType.SEMICOLON, None)
            case '*':
                self.add_token(TokenType.STAR, None)
            case '!':
                self.add_token(TokenType.BANG_EQUAL if self.match('=') else TokenType.BANG, None)
            case '=':
                self.add_token(TokenType.EQUAL_EQUAL if self.match('=') else TokenType.EQUAL, None)
            case '<':
                self.add_token(TokenType.LESS_EQUAL if self.match('=') else TokenType.LESS, None)
            case '>':
                self.add_token(TokenType.GREATER_EQUAL if self.match('=') else TokenType.GREATER, None)
            case '/':
                if self.match('/'):
                    while self.peek() != '\n' and not self.is_at_end():
                        self.advance()
                else:
                    self.add_token(TokenType.SLASH, None)
            case ' ' | '\r' | '\t':
                pass
            case '\n':
                type(self).line += 1
            case '"':
                self.string()
            case _:
                if self.is_digit(c):
                    self.number()
                elif self.is_alpha(c):
                    self.identifier()
                else:
                    error(type(self).line, 'Unexpected character.')


    def scan_tokens(self):
        while not self.is_at_end():
            type(self).start = type(self).current
            self.scan_token()
        type(self).tokens.append(Tokens(TokenType.EOF, '', None, type(self).line))
        return type(self).tokens

