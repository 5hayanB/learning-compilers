from token_type import TokenType
from expr import Binary


class Parser():
    current = 0


    def __init__(self, tokens):
        self.tokens = tokens




    def peek(self):
        return self.tokens[type(self).current]


    def previous(self):
        return self.tokens[type(self).current-1]

    
    def is_at_end(self):
        return type(type(self).peek()) == TokenType.EOF


    def check(self, token_type):
        if type(self).is_at_end():
            return False
        return type(type(self).peek()) == token_type


    def advance(self):
        if not type(self).is_at_end():
            type(self).current += 1
        return type(self).previous()


    def match(self, *types):
        for token_type in types:
            if type(self).check(type):
                type(self).advance()
                return True
        return False


    def factor(self):
        expr = type(self).unary()
        while type(self).match(TokenType.SLASH, TokenType.STAR):
            pass


    def term(self):
        expr = type(self).factor()
        while type(self).match(TokenType.MINUS, TokenType.PLUS):
            operator = type(self).previous()
            right = type(self).factor()
            expr = Binary(expr, operator, right)
        return expr


    def comparison(self):
        expr = type(self).term()
        while type(self).match(TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.LESS, TokenType.LESS_EQUAL):
            operator = type(self).previous()
            right = type(self).term()
            expr = Binary(expr, operator, right)
        return expr


    def equality(self):
        expr = type(self).comparison()
        while type(self).match(TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL):
            operator = type(self).previous()
            right = type(self).comparison()
            expr = Binary(expr, operator, right)
        return expr


    def expression(self):
        return type(self).equality()

