import sys, os


def define_visitor(fd, base_name, class_types):
    fd.write('class Visitor(ABC):\n')
    for class_type in class_types:
        class_name = class_type.split(':')[0].strip()
        fd.write('    @abstractmethod\n')
        fd.write(f'    def visit_{class_name.lower()}_{base_name.lower()}(self, {base_name.lower()}):\n')
        fd.write('        pass\n\n\n')


def define_type(fd, base_name, class_name, field_list):
    fd.write(f'class {class_name}({base_name}):\n')
    fd.write(f'    def __init__(self, {field_list}):\n')
    fields = field_list.split(', ')
    for field in fields:
        fd.write(f'        self.{field} = {field}\n')
    else:
        fd.write('\n\n')
    fd.write('    def accept(self, visitor):\n')
    fd.write(f'        return visitor.visit_{class_name.lower()}_{base_name.lower()}(self)\n\n\n')


def define_ast(output_dir, base_name, class_types):
    path = os.path.join(output_dir, f'{base_name.lower()}.py')
    with open(path, 'w', encoding='utf-8') as f:
        f.write('from abc import ABC, abstractmethod\n\n\n')
        define_visitor(f, base_name, class_types)
        f.write(f'class {base_name}(ABC):\n')
        f.write('    @abstractmethod\n')
        f.write('    def accept(self, visitor):\n')
        f.write('        pass\n\n\n')
        for class_type in class_types:
            class_name = class_type.split(':')[0].strip()
            fields = class_type.split(':')[1].strip()
            define_type(f, base_name, class_name, fields)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: python3 gen_ast.py <output directory>')
        sys.exit(64)
    output_dir = os.path.abspath(sys.argv[1])
    define_ast(output_dir, 'Expr', ['Binary : left, operator, right', 'Grouping : expression', 'Literal : value', 'Unary : operator, right'])

