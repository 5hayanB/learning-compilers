had_error = False


def report(line, where, msg):
    print(f'[line {line}] Error{where}: {msg}')
    global had_error
    had_error = True


def error(line, msg):
    report(line, '', msg)

